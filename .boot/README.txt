---------------------------------------------------------
                 UEFI BOOT LOADING NOTES
---------------------------------------------------------

* The MSI x299 UEFI firmware UI doesn't seem to permit a 
  serviceable configuration for booting Arch Linux in 
  modern UEFI mode. It can't seem to find the drive with
  an EFI partition (i.e., /dev/nvme0n1p1).

* Arch Linux allows for booting the kernel without a boot
  loader; this is a very clean way to launch the OS. The
  wiki at wiki.archlinux.org/index.php/EFISTUB has more
  details.

* To get this to work with the MSI x299 motherboard, we
  need to bypass the motherboard's EFI GUI and configure
  the NVRAM on the motherboard directly.

* There are (at least) two ways to do this. The first is
  using the `efibootmgr` tool in an Arch shell. To do this,
  boot into the Arch Live thumbdrive, mount /mnt and 
  mnt/boot, and run the command as described in the Arch
  Linux wiki for EFISTUB. This did *not* work for some 
  reason; the changes didn't persist. So we were left with
  the second way to configure the NVRAM directly: the UEFI 
  shell.

* There are two versions of the UEFI shell. V1 can be 
  launched directly from the x299's UEFI GUI, but it 
  does not have the tool we need. We need to launch UEFI
  shell V2, which we do from the Arch Linux live thumbdrive.
  When we have the thumbdrive installed and F11 into boot
  options on powerup, there are a few options provided by
  the Arch iso; one is UEFI SHell V2.

* In the V2 shell, we use the `bcfg` tool to add an entry
  to the boot list. Instructions are on the EFISTUB wiki,
  but in general we first add an entry for Arch Linux:

  Shell> bcfg boot add 0 fs2:\vmlinuz-linux "Arch Linux"

* The "0" means first boot position, and we get the "fs2"
  from `map` (confirm by looking at what is on the drive:
  
  Shell> ls fs2:
  
  If you see the contents of your boot partition, that's 
  the drive. 

  To see what currently exists in the boot config, use 
  `bcfg boot dump`

* After adding the new boot entry to Arch Linux, we need 
  to tell it to use a file we made in /boot/efi-options.txt
  for kernel options:

  Shell> bcfg boot -opt 0 fs2:\efi-options.txt

* This will persist the boot entry in the CMOS chip and 
  when the system is restarted, UEFI will launch the Arch
  kernel without a special boot loader. See the wiki for
  further details.
  
