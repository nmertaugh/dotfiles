# DEFAULT TEXT EDITOR
export EDITOR='code'
export BUNDLER_EDITOR='code'

# UNLIMITED SHELL HISTORY
export HISTFILE= # nil means unlimited
export HISTFILESIZE= # nil means unlimited
export HISTSIZE= # nil means unlimited

# IGNORE DUPLICATE ENTRIES IN HISTORY
export HISTIGNORE="&"

# COLORIZE SHELL PROMPT
export CLICOLOR='Yes'
eval "$(dircolors /home/nmertaugh/.dir_colors)"
export PS1='\n\[\e[2;37m\]\t \[\033[0;32m\]\H\[\033[0m\]:\[\033[0;34m\]\W\[\033[00m\] \$ '

# LIST OF COLOR CODES FOR PROMPT:
#
#   Black        0;30     Dark Gray     1;30  (2;XX will dim the color)
#   Red          0;31     Light Red     1;31
#   Green        0;32     Light Green   1;32
#   Brown/Orange 0;33     Yellow        1;33
#   Blue         0;34     Light Blue    1;34
#   Purple       0;35     Light Purple  1;35
#   Cyan         0;36     Light Cyan    1;36
#   Light Gray   0;37     White         1;37
#
# Example usage:
#   RED='\033[0;31m'
#   NC='\033[0m' # No Color
#
#   printf "I ${RED}love${NC} Merissa\n"

# COLORIZE DIRECTORY LISTINGS
# (colors; uppercase = bold)
# a = black
# b = red
# c = green
# d = brown
# e = blue
# f = magenta
# g = cyan
# h = light gray
# x = default

# (order; each entry is a foreground|background pair)
# DIR
# SYM_LINK
# SOCKET
# PIPE
# EXE
# BLOCK_SP
# CHAR_SP
# EXE_SUID
# EXE_GUID
# DIR_STICKY
# DIR_WO_STICKY

# SET PATH
paths="
  $HOME/.rbenv/shims
  $HOME/.rbenv/bin
  /usr/local/heroku/bin
  $HOME/bin
  /usr/local/bin
  /usr/local/sbin
  /usr/local/mysql/bin
  /usr/bin
  /usr/sbin
  /bin
  /sbin
"
export PATH=$(echo $paths | sed s/[[:blank:]]/:/g)

# RB-ENV
eval "$(rbenv init -)"

# Never ever load Spring
export DISABLE_SPRING=true

if [ -f "${HOME}/.bash_profile" ]; then
  source "${HOME}/.bash_profile"
fi

# Readline completion
bind TAB:menu-complete

# Set globstar and key repeat rates
shopt -s globstar

# Run screenfetch to show system stats
screenfetch

# Set key repeat speed and delay
key.set 400 30

# Node Version Manager
source /usr/share/nvm/init-nvm.sh
