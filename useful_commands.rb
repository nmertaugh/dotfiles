COMMANDS = {
  'uname -a' => 'list info about Linux distro',
  'lsblk' => 'list block device info',
  'sudo pacman -Syu' => 'update all packages',
  'md5sum <path/to/file>' => 'generate MD5 checksum',
  'sha1sum <path/to/file>' => 'generate SHA1 checksum',
  'ip addr' => 'arch version of ifconfig, shows interfaces and IPs',
  'sudo arp-scan --interface=<INTERFACE> --localnet' => 'show local IPs (INTERFACE is from `ip addr`)' ,
  'scanimage -L' => 'list scanners',
  'lsusb' => 'list devises connected by USB',
  'lpstat -a' => 'list printers',
  'lsmod' => 'list modules kernel currently has loaded',
  'modinfo <module_name>' => 'info about given module',
  'systool -v -m <module_name>' => 'list settings for a given module',
  'systemctl is-enabled <module_name>' => 'see if a module is enabled',
  'cd-info' => 'list info about CD-ROM drive',
  'groups <username>' => 'list groups of which user is a member',
  'getent group <groupo_name>' => 'list members that are a member of specified group',
  'cut -d: -f1 /etc/group' => 'list all unix groups on system',
  'checkupdates' => 'show pacman packages that are out of date',
  'ls /usr/share/gnome-control-center/keybindings' => 'show keybinding files',
  'pdfunite page_1.pdf page_2.pdf output_file.pdf' => 'merges PDF documents into one file',
  'lsdvd -x /run/media/nmertaugh/Strunz\ &\ Farah\ DVD' => 'outputs data on DVD in drive',
  'youtube-dl -x --audio-format mp3 --audio-quality 0 -k <video URL>' => 'download YouTube audio',
  'cd-info --dvd' => 'info on DVD disc and drive',
  'find /path/to/base/dir -type f -exec chmod 644 {} +' => 'Recursively give files read privileges',
  'find /path/to/base/dir -type d -exec chmod 755 {} +' => 'Recursively give directories read & execute privileges',
  'backblaze-b2 sync --delete LOCAL_DIRECTORY b2://BUCKET_NAME' => 'Bring B2 bucket in sync with local directory',
  'yaourt -Su --aur' => 'update all AUR packages',
  'cat /etc/pacman.conf' => 'print pacman config file',
  'sudo pacman -S archlinux-keyring' => 'if pacman update complains about untrusted signatures',
  'wifi-menu' => 'connect to a wifi network via the command line',
  'lynx <URL>' => 'command line web browser',
  'cat /etc/fstab' => 'view the file system table (used to mount drives during bootup)',
  'lsblk -o +UUID' => 'list block devices (i.e., drives, UUIDs, and mount points)',
  'mkinitcpio -p linux' => 'rebuild kernel images in /boot',
  'blkid' => 'get PARTUUIDs and file system types for mounted drives',
  'cat /etc/mkinitcpio.conf' => 'view config for mkinitcpio command',
  'cat /boot_example/efibootmgr_command.sh' => 'view efibootmgr command for setting boot order etc',
  'sudo efibootmgr -v' => 'examine boot order',
  'fdisk -l' => 'list devices (drives) and metadata about them, including device path in /dev',
  'dmesg' => 'print message buffer of kernel',
  'sudo iotop' => 'show disk I/O usage',
  'sudo hdparm -B 254 /dev/sda1' => 'try to increase hard drive performance',
  'xrandr' => 'show information about all connected monitors',
  'lspci -k | grep -A 2 -E "(VGA|3D)"' => 'show information about graohics cards',
  'gpg --verify some_file.sig some_file.foo' => 'verifies validity of file',
  'gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify some_file.sig some_file.foo' => 'verifies validity of file when you do not have the public key (fetches it)',
  'gpg --recv-keys PUBLIC_KEY_ID' => 'import key for author via their public key ID (found via Internet search)' ,
  'dd bs=4M if=/path/to/archlinux.iso of=/dev/FLASH_DRIVE_NUM status=progress oflag=sync' => 'make bootable install USB drive; check FLASH_DRIVE_NUM with `fdisk -l`, but OMIT the partition number',
  'convert input_file.jpg -resize 48x48 output_file.jpg' => 'resize an image',
}

SORTED_COMMANDS = COMMANDS.keys.sort.each_with_object({}) do |key, hash|
  hash[key] = COMMANDS[key]
end

def colorize(string, color = :green)
  color_code =
    case color
    when :blue
      34
    when :green
      32
    when :light_blue
      36
    when :yellow
      33
    end

  "\e[#{color_code}m#{string}\e[0m"
end

def padding_for(command)
  min_padding = 5
  max_length = COMMANDS.keys.map(&:length).max + min_padding
  padding_length = max_length - command.length

  Array.new(padding_length) { ' ' }.join
end

SORTED_COMMANDS.each_pair do |command, description|
  puts "#{colorize(command, :light_blue)} #{padding_for(command)}(#{description})"
end
