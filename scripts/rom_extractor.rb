# Rename Files and directories
def rename
  pwd = Dir.pwd

  Dir.entries(".").reject { %w[. ..].include?(_1) }.each do |old_name|
    new_name = old_name.gsub(/_/, " ").gsub(" .", ".")

    next if new_name == old_name

    puts " Moving #{pwd}/#{old_name} -> #{pwd}/#{new_name}"
    `mv "#{pwd}/#{old_name}" "#{pwd}/#{new_name}"`
  end
end

def extract_roms(extension)
  pwd = Dir.pwd

  games_directory = "#{pwd}/AAA-FLASHCART-ROMS"

  unless File.exist?(games_directory)
    `mkdir "#{games_directory}"`
  end

  processed_directory = "#{pwd}/AAA-PROCESSED-DIRECTORIES"

  unless File.exist?(processed_directory)
    `mkdir "#{processed_directory}"`
  end

  entries_to_skip = [
    ".",
    "..",
    "AAA-FLASHCART-ROMS",
    "AAA-PROCESSED-DIRECTORIES"
  ]

  Dir.entries(".").reject { entries_to_skip.include?(_1) }.each do |entry|
    game_directory_name = Pathname([pwd, entry].join("/"))

    if game_directory_name.directory?
      Dir["#{game_directory_name.expand_path}/*.#{extension}"].each do |rom_file|
        rom_file_name = rom_file.split("#{game_directory_name}/").last
        shortened_rom_name = rom_file_name.gsub(/\(.*\)/, "").strip.gsub(" .", ".")

        puts shortened_rom_name
        `cp "#{rom_file}" "#{games_directory}/#{shortened_rom_name}"`
        `mv "#{game_directory_name.expand_path}" "#{processed_directory}/#{entry}"`
      end
    end
  end
end
extract_roms("md")

# ROMs (sms md nes sfc z64)
def unzip_all(extension)
  pwd = Dir.pwd
  games_directory = "#{pwd}/AAA-FLASHCART-ROMS"

  unless File.exist?(games_directory)
    `mkdir "#{games_directory}"`
  end

  zips_directory = "#{pwd}/AAA-ZIP-FILES"

  unless File.exist?(zips_directory)
    `mkdir "#{zips_directory}"`
  end

  Dir["#{pwd}/*.zip"].each do |zip_file|
    zip_file_name = zip_file.split("#{pwd}/").last
    game_directory_name = zip_file_name.gsub(/\.zip\Z/, "").gsub(/_/, " ")
    shortened_file_name = game_directory_name.gsub(/\(.*\)/, "").strip

    puts "Unzipping #{zip_file_name}..."
    `unzip "#{zip_file_name}" -d "#{game_directory_name}"`

    puts " Unzipping complete!"
    puts " Moving #{zip_file_name} -> #{zips_directory}/#{zip_file_name}"
    `mv "#{zip_file_name}" "#{zips_directory}/#{zip_file_name}"`

    puts " Move complete!"

    Dir["#{game_directory_name}/*.#{extension}"].each do |rom_file|
      rom_file_name = rom_file.split("#{game_directory_name}/").last
      rom_base_name = rom_file_name.gsub(/\.#{extension}\Z/, "")
      shortened_rom_name = rom_base_name.gsub(/\(.*\)/, "").strip

      puts " Copying #{rom_file} -> #{games_directory}/#{shortened_rom_name}.#{extension}"
      `cp "#{rom_file}" "#{games_directory}/#{shortened_rom_name}.#{extension}"`

      puts " Copy complete!"
    end
  end
end
unzip_all("z64")

# Nintendo DS/GB (zip files with single rom file inside)
def unzip_all(extension)
  pwd = Dir.pwd

  zips_directory = "#{pwd}/AAA-ZIP-FILES"

  unless File.exist?(zips_directory)
    `mkdir "#{zips_directory}"`
  end

  Dir["#{pwd}/*.zip"].each do |zip_file|
    zip_file_name = zip_file.split("#{pwd}/").last
    rom_base_name = zip_file_name.gsub(/\.zip\Z/, "")
    shortened_file_name = rom_base_name.gsub(/\(.*\)/, "").strip

    puts "Unzipping #{zip_file_name}..."
    `unzip "#{zip_file_name}"`
    puts " Unzipping complete!"

    puts " Moving #{zip_file_name} -> #{zips_directory}/#{zip_file_name}"
    `mv "#{zip_file_name}" "#{zips_directory}/#{zip_file_name}"`
    puts " Move complete!"

    puts " Renaming #{rom_base_name}.#{extension} -> #{shortened_file_name}.#{extension}"
    `mv "#{rom_base_name}.#{extension}" "#{shortened_file_name}.#{extension}"`
    puts " Copy complete!"
  end
end
unzip_all("gb")

# Gamecube .ciso or .nkit.iso files (iso inside 7zip)
def unzip_all
  pwd = Dir.pwd
  zips_directory = "#{pwd}/AAA-7ZIP-FILES"

  unless File.exist?(zips_directory)
    `mkdir "#{zips_directory}"`
  end

  Dir["#{pwd}/*.7z"].each do |zip_file|
    zip_file_name = zip_file.split("#{pwd}/").last
    original_game_directory_name = zip_file_name.gsub(/\.7z\Z/, "")
    shortened_directory_name =
      original_game_directory_name.gsub(/\((?![D|d]isc \d+\)).*?\)/, "").strip.gsub(/  +/, " ")

    puts "Un-archiving #{original_game_directory_name}..."

    system %[7z -bsp1 x "#{zip_file_name}" -x!"Vimm's Lair.txt"]

    puts " Finished un-archiving!"
    puts " Moving #{original_game_directory_name} -> #{shortened_directory_name}..."

    `mv "#{original_game_directory_name}" "#{shortened_directory_name}"`
    `mv "#{zip_file_name}" "#{zips_directory}/#{zip_file_name}"`

    puts " Finished moving!"
  end
end
unzip_all

# BINs / CUEs
def unzip_all
  pwd = Dir.pwd
  zips_directory = "#{pwd}/AAA-7ZIP-FILES"

  unless File.exist?(zips_directory)
    `mkdir "#{zips_directory}"`
  end

  Dir["#{pwd}/*.7z"].each do |zip_file|
    zip_file_name = zip_file.split("#{pwd}/").last
    original_game_directory_name = zip_file_name.gsub(/\.7z\Z/, "")
    shortened_directory_name =
      original_game_directory_name.gsub(/\((?![D|d]isc \d+\)).*?\)/, "").strip.gsub(/  +/, " ")

    puts "Un-archiving #{original_game_directory_name}..."

    system %[7z -bsp1 x "#{zip_file_name}" -x!"Vimm's Lair.txt"]

    puts " Finished un-archiving!"
    puts " Moving #{original_game_directory_name} -> #{shortened_directory_name}..."

    `mv "#{original_game_directory_name}" "#{shortened_directory_name}"`
    `mv "#{zip_file_name}" "#{zips_directory}/#{zip_file_name}"`

    puts " Finished moving!"

    Dir[
      # "#{shortened_directory_name}/*.bin",
      "#{shortened_directory_name}/*.cue"
    ].each do |bin_or_cue_file|
      old_file_name = bin_or_cue_file
      new_file_name =
        old_file_name.gsub(original_game_directory_name, shortened_directory_name)

      puts " Renaming #{old_file_name} -> #{new_file_name}"

      `mv "#{old_file_name}" "#{new_file_name}"`
    end
  end
end
unzip_all

# PS3
def ps3_iso
  pwd = Dir.pwd
  zips_directory = "#{pwd}/AAA-7ZIP-AND-DKEY-FILES"

  unless File.exist?(zips_directory)
    `mkdir "#{zips_directory}"`
  end

  isos_directory = "#{pwd}/AAA-ISO-FILES"

  unless File.exist?(isos_directory)
    `mkdir "#{isos_directory}"`
  end

  Dir["#{pwd}/*.7z"].each do |zip_file|
    zip_file_name = zip_file.split("#{pwd}/").last
    original_game_directory_name = zip_file_name.gsub(/\.7z\Z/, "")
    shortened_directory_name =
      original_game_directory_name.gsub(/\((?![D|d]isc \d+\)).*?\)/, "").strip.gsub(/  +/, " ")

    puts "Un-archiving #{original_game_directory_name}..."
    system %[7z -bsp1 x "#{zip_file_name}" -x!"Vimm's Lair.txt"]

    puts " Creating iso file... "
    system %[makeps3iso "#{original_game_directory_name}" "#{isos_directory}/#{shortened_directory_name}.iso"]

    puts " Moving zip file and dkey..."
    `mv "#{zip_file_name}" "#{zips_directory}/#{zip_file_name}"`
    `mv "#{original_game_directory_name}.dkey" "#{zips_directory}/#{original_game_directory_name}.dkey"`

    puts " Removing unzipped game directory..."
    `rm -rf "#{original_game_directory_name}"`
  end
end
ps3_iso

# Dumped PS1 (dump via Retroarch "Dump Disc" functionality, find in ~/.config/retroarch/downloads)
def rename_bin_cue
  pwd = Dir.pwd
  game_name = pwd.split("/").last

  cue_file_path = Dir["#{pwd}/*.cue"].first
  original_cue_file_name = cue_file_path.split("/").last.gsub(".cue", "")

  puts "Sedding track names in cue file"
  `sed -i 's/#{original_cue_file_name}/#{game_name}/g' '#{cue_file_path}'`

  puts "Renaming #{original_cue_file_name}.cue -> '#{game_name}.cue'"
  `mv "#{cue_file_path}" "#{pwd}/#{game_name}.cue"`

  puts "Renaming bin files"
  Dir["#{pwd}/*.bin"].each do |bin_file_path|
    bin_file_name = bin_file_path.split("/").last
    new_bin_file_name = bin_file_name.gsub(original_cue_file_name, game_name)

    `mv "#{bin_file_path}" "#{pwd}/#{new_bin_file_name}"`
  end
end
rename_bin_cue
