require 'yaml'

class Mp3Splitter
  Timestamp = Struct.new(:start, :end, :track_name, keyword_init: true)

  attr_reader :source_directory_path, :source_file_path, :timestamps

  def initialize(source)
    @source_file_path = source
    @source_directory_path = source.split(/\/[a-z|_]+\.mp3$/).first
    @timestamps = load_timestamps
  end

  def split
    # ffmpeg -i long_track.mp3 -vn -acodec copy -ss 58:17 -to 59:13.999 new_track.mp3
    timestamps.each do |timestamp|
      command = %(ffmpeg -i "#{source_file_path}" -vn -acodec copy #{timestamp.start} ) +
        %(#{timestamp.end} "#{source_directory_path}/#{timestamp.track_name}")
      puts command
      `#{command}`
    end

    nil
  end

  private

  def load_timestamps
    raw_timestamps = YAML.load(File.read("#{source_directory_path}/timestamps.yml"))
    num_timestamps = raw_timestamps.length
    parsed_timestamps = raw_timestamps.map { |raw_timestamp| raw_timestamp.split(' - ') }

    parsed_timestamps.each_with_index.map do |parsed_timestamp, index|
      next_start = (index == num_timestamps - 1) ? nil : parsed_timestamps[index + 1][0]

      Timestamp.new(
        start: "-ss #{parsed_timestamp[0]}.000",
        end: next_start.nil? ? '' : "-to #{subtract_one_second_from(next_start)}.999",
        track_name: "#{pad(index + 1)} - #{parsed_timestamp[1]}.mp3",
      )
    end
  end

  def pad(int)
    "%02d" % int
  end

  def subtract_one_second_from(timestamp, position_from_end = 0)
    time_pieces = timestamp.split(':')
    target_time = time_pieces[time_pieces.length - position_from_end - 1].to_i

    if target_time.zero?
      time_pieces[time_pieces.length - position_from_end - 1] = '59'
      new_timestamp = time_pieces.join(':')
      subtract_one_second_from(new_timestamp, position_from_end + 1)
    else
      time_pieces[time_pieces.length - position_from_end - 1] = pad(target_time - 1)
      time_pieces.join(':')
    end
  end
end
