# ===================== PaperTrail (with facility name) =====================
facility_name = "Rolla"
facility = Facility.find_by(name: facility_name)
location = facility.center_location
employee_first_name = "Patrick"
employee_last_name = "Stites"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}

# ===================== Common objects =====================
donor_id = 168713
donor = facility.donors.find(donor_id)
donation_id = 1311232
donation = donor.visits.find(donation_id)
queued_donor, screening, physical, setup, phlebotomy, disconnect, unit, sample =
  donation.slice(
    :queued_donor,
    :screening,
    :physical,
    :setup,
    :phlebotomy,
    :disconnect,
    :unit,
    :sample
  ).values

# ===================== Sanity check =====================
donor == queued_donor.donor &&
  donation.donor == donor &&
  screening == donation.screening &&
  queued_donor = donation.queued_donor
  setup == donation.setup &&
  disconnect == donation.disconnect &&
  phlebotomy == donation.phlebotomy &&
  unit == donation.unit &&
  sample == unit&.sample &&
  sample_review == donation&.sample_review

# TEMP
donation.sample_review.attributes.slice "id", "spe_physician_requires_recollection"
donation.sample_review.update! spe_physician_requires_recollection: false

# ===================== Switch facilities =====================
facility_name = "Tahlequah"
facility = Facility.find_by(name: facility_name)
location = facility.center_location
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}

# ===================== Test results with paper trail =====================
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
employee_first_name = "Eva"
employee_last_name = "Anderson"
employee = Employee.find_by(
  first_name: employee_first_name,
  last_name: employee_last_name
)

donor_id_ucn_map = {
  50326 => "GI1000014666",
  122889 => "GI1000014714",
  60337 => "TO1000012015",
  140048 => "TO1000012032",
}

donor_id_ucn_map.each_pair do |donor_id, ucn|
  donor = Donor.find(donor_id)
  facility = donor.facility
  location = facility.center_location

  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }

  puts PaperTrail.request.controller_info

  unit = donor.units.find_by(control_number: ucn)
  sample = unit.sample
  test_results = TestResult
    .joins(:test_result_scores)
    .where(test_result_scores: { unit_control_number: ucn })
    .uniq
  test_results.each do |test_result|
    TestResultScore::Pivot.new(test_result: test_result).save
  end
end


# ===================== Nullify phone number =====================
old_phone_number = "+15013221912"
new_phone_number = nil
account = Account.find_by(phone_number: old_phone_number)
donor = account.donor
facility = donor.facility
location = facility.center_location
employee_first_name = "Eva"
employee_last_name = "Anderson"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}
account.update! phone_number: new_phone_number


# ===================== UPDATE CASE LABEL =====================
shipment = Shipment.find(276) # from URL Luke pasted in Slack: https://app.buoysoftware.com/locations/11/casing/shipments/276/bill-of-lading
plasma_case = shipment.plasma_cases.first # there is only one
plasma_case.update! label_id: "100014211899"


# ===================== Update case label by old label =====================
plasma_case = facility.plasma_cases.find_by(label_id: "10072200083")
plasma_case.attributes.slice "id", "label_id"
plasma_case.update! label_id: "100072200087"
plasma_case.reload.attributes.slice "id", "label_id"


# ===================== Incorrect screening equipment =====================
screening.screening_equipment.map { |x| [x.equipment.serial_number, x.equipment.equipment_type] }

# incorrect_weight_scale = Equipment.find_by(serial_number: "ws155800012")
# correct_weight_scale = Equipment.find_by(serial_number: "ws15580012")

incorrect_vital_signs_monitor = Equipment.find_by(serial_number: "vs155800013")
correct_vital_signs_monitor = Equipment.find_by(serial_number: "vs15580013")

incorrect_hematastat = Equipment.find_by(serial_number: "hs155800011")
correct_hematastat = Equipment.find_by(serial_number: "hs15580011")

# weight_scale_association = screening.screening_equipment.find_by(equipment: incorrect_weight_scale)
# weight_scale_association.update! equipment: correct_weight_scale

vital_signs_association = screening.screening_equipment.find_by(equipment: incorrect_vital_signs_monitor)
vital_signs_association.update! equipment: correct_vital_signs_monitor

hematastat_association = screening.screening_equipment.find_by(equipment: incorrect_hematastat)
hematastat_association.update! equipment: correct_hematastat

screening.reload.screening_equipment.map { |x| [x.equipment.serial_number, x.equipment.equipment_type] }


# ===================== UPDATE CONSIGNEE ===========================
def set_papertrail(facility)
  location = facility.center_location
  employee_first_name = "Ticket author first name"
  employee_last_name = "Ticket author last name"
  employee = Employee.find_by(first_name: employee_first_name, last_name: employee_last_name)
  engineer = Account.find_by(email: "YOUR_EMAIL@buoysoftware.com")
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }
end

def update_shipment(shipment)
  facility = shipment.consignee_facility.facility
  set_papertrail facility

  old_consignee_name = "BioLife Plasma Services, LLC – c/o RXCrossroads, Erlanger, KY"
  new_consignee_name = "BioLife Plasma Services - Plasma Operations"

  old_consignee = Consignee.find_by(name: old_consignee_name)
  new_consignee = Consignee.find_by(name: new_consignee_name)
  old_consignee_facilities = old_consignee
    .consignee_facilities
    .map { [_1.facility.name, _1.id] }
    .to_h
  new_consignee_facilities = new_consignee
    .consignee_facilities
    .map { [_1.facility.name, _1.id] }
    .to_h

  return_hash = {}
  return_hash[:before] = shipment
    .consignee_facility
    .attributes
    .slice("id", "consignee_id", "product_types", "status")
    .tap { _1["consignee"] = shipment.consignee.name }

  shipment.update! consignee_facility_id: new_consignee_facilities[facility.name]

  return_hash[:after] = shipment
    .reload
    .consignee_facility
    .attributes
    .slice("id", "consignee_id", "product_types", "status")
    .tap { _1["consignee"] = shipment.consignee.name }

  return_hash
end

# This will vary depending on ticket of course:
kingsville_shipment = Shipment.find(527)
el_dorado_shipment = Shipment.find(468)
brownwood_shipment = Shipment.find(517)

update_shipment(kingsville_shipment)
update_shipment(el_dorado_shipment)
update_shipment(brownwood_shipment)

# ===================== UPDATE TEST NUMBER =====================
def set_papertrail(facility: nil, donation_id: nil)
  raise "NOPE" unless [facility, donation_id].any?(&:present?)
  facility = facility || Donation.find(donation_id).facility
  location = facility.center_location
  employee_first_name = "Luke"
  employee_last_name = "Winter"
  employee = Employee.find_by(first_name: employee_first_name, last_name: employee_last_name)
  engineer = Account.find_by(email: "nicholas@buoysoftware.com")
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }
end

def update_test_number(data:)
  sample = Sample.find_sole_by(data[:type] => data[:wrong])
  facility = sample&.visit&.facility
  if facility
    set_papertrail(facility:)
    return_hash = { sample_before: sample.attributes.slice("id", data[:type].to_s, donation_id: sample.visit.id) }
    sample.update! data[:type] => data[:right]
    return_hash[:sample_after] = sample.reload.attributes.slice("id", data[:type].to_s, donation_id: sample.visit.id)
    return_hash
  else
    raise "NOPE"
  end
end
data = { type: :vmt, wrong: "VS1000004410N", right: "VS1000004410V" }
update_test_number data:

# ===================== END DEFERRAL =====================
terminator = employee
donor_id = 7114
donor = facility.donors.find(donor_id)
deferral_id = 3795
deferral = donor.donor_deferrals.find(deferral_id)
# ends_on = deferral.starts_on + 2.days
ends_on = Date.parse("2024-06-24")
comment = deferral.comment + " -- UPDATE: Deferral end date updated per change request #2084"
deferral.update!(ends_on:, terminator:, comment:)
DonorDeferral::DeferralLiftedMessage.publish(deferral)

# ====================== UPDATE WEIGHT ============================
weight = 250
weight_acceptable = true
Screening::Weight.new(queued_donor:, actor: employee, weight:, weight_acceptable:).save
# This updates screening and donor records
# Check that screening has correct volume tier, Screening::Weight does not update that:
screening.volume_tier == VolumeTier.by_weight(weight).tier_name
# If not, then update screening:
screening.update! volume_tier: VolumeTier.by_weight(weight).tier_name






BuoySoftware::Auth0Client.build.users_by_email "charlene+suspended1@buoysoftware.com"











BuoySoftware::Auth0Client.build.unlink_user_account "auth0|115", "samlp", "BUOY-SP|nicholas@buoysoftware.com" # (note secoindary account ID has no prepended provider)






donor = Donor.create!(facility: Facility.first, account: Account.create!)
Rig::DJ::Donation.complete_through donor:, employee: Employee.first, target_step: Disconnect::STEPS.last

def generate_ssn
  digits = Array.new(9) { rand(9) }.join
  [digits[0..2], digits[3..4], digits[5..]].join("-")
end

def set_up_paper_trail(facility:)
  engineer = Account.find_by(email: "nicholas@buoysoftware.com") || Account.first
  employee = engineer.employee
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: facility.center_location.id,
  }
end
alias :supt :set_up_paper_trail

def create_qualified_donor(n: 1, facility:)
  ActiveRecord::Base.transaction do
    set_up_paper_trail(facility:)
    location = facility.center_location

    account = Account.where(
      email: "nicholas-donor-reset-#{n}@example.com"
    ).first_or_create!

    donor = Donor.where(
      first_name: "Nicholas",
      last_name: "DonorFloorReset #{n}",
      classification: "applicant",
      account:,
      facility:,
      # classification: :qualified,  # omit to see in DQ
      # last_qualified_at: 200.days.ago # omit to see in DQ
    ).first_or_create!
    ssn = generate_ssn

    until Registration.where(ssn: ssn).none?
      ssn = generate_ssn
    end

    donor.create_registration!(ssn: ssn, sex: "male") if donor.registration.blank?

    [200.days.ago, 30.days.ago].each do |visit_at|
      donation = donor.donations.create!(
        created_at: visit_at,
        facility:,
        location:,
        scheduled_at: visit_at,
        status: "completed",
        checked_in_at: visit_at + 10.minutes,
        completed_at: visit_at + 1.hour,
        donation_in_milliliters: 600,
        phlebotomy_started_at: visit_at + 15.minutes
      )
      donation.create_queued_donor!(
        created_at: visit_at,
        donor_queue: "disconnect",
      )
      donation.create_quality_control_review!(
        created_at: visit_at,
        vitals_acceptable: true,
        vitals_review_completed_at: visit_at + 10.minutes,
        physical_review_completed_at: visit_at + 10.minutes,
        donation_history_acceptable: visit_at + 10.minutes,
        donation_history_completed_at: visit_at + 10.minutes,
        questionnaire_review_completed_at: visit_at + 10.minutes,
        consent_review_completed_at: visit_at + 10.minutes,
        completed_at: visit_at + 10.minutes,
        test_results_acceptable: true,
        test_history_review_completed_at: visit_at + 10.minutes,
        physical_confirmed: true,
        questionnaire_confirmed: true,
        consent_confirmed: true
      )
      screening = donation.create_screening!(
        created_at: visit_at,
        questionnaire_acceptable: true,
        latest_pulse_acceptable: true,
        latest_blood_pressure_acceptable: true,
        weight_acceptable: true,
        temperature_acceptable: true,
      )
      physical = donation.create_physical!(
        created_at: visit_at,
        physical_acceptable: true,
        completed_at: visit_at + 1.minute,
        hands_on_physical_completed_at: visit_at + 1.minute,
        nddr_acceptable: true,
        nddr_code: "246789",
      )
      ucn = "#{facility.prefix}#{Array.new(10) { rand(9) }.join}"
      unit = donation.create_unit!(
        created_at: visit_at,
        control_number: ucn,
        status: "tested",
      )
      sample = unit.create_sample!(
        created_at: visit_at,
        volume_acceptable: true,
        volume_completed_at: visit_at + 1.hour,
        overdraw_or_underdraw_event: false,
        donation_events_completed_at: visit_at + 1.hour,
        vmt: "#{ucn}v",
        nat: "#{ucn}n",
        spe: "#{ucn}s",
        atya:"#{ucn}a" ,
        required_samples_collected: true,
        required_samples_comment: nil,
        tests_completed_at: visit_at + 1.hour,
        unit_status_completed_at: visit_at + 1.hour,
        completed_at: visit_at + 1.hour,
        unit_status: "acceptable",
      )
      unit.create_sample_review!(
        created_at: visit_at,
        vmt_requires_donor_deferral: false,
        nat_acceptable: true,
        nat_completed_at: visit_at + 1.hour,
        vmt_acceptable: true,
        vmt_completed_at: visit_at + 1.hour,
        atya_acceptable: true,
        atya_completed_at: visit_at + 1.hour,
        created_at: visit_at + 1.hour,
        spe_physician_acceptable: true,
        spe_physician_requires_recollection: false,
        spe_physician_requires_donor_deferral: false,
        spe_physician_completed_at: visit_at + 1.hour,
        spe_quality_acceptable: true,
        spe_quality_requires_recollection: false,
        spe_quality_completed_at: visit_at + 1.hour,
        atya_unit_status_completed_at: visit_at + 1.hour,
        atya_review_completed_at: visit_at + 1.hour,
        vmt_unit_status_completed_at: visit_at + 1.hour,
        vmt_review_completed_at: visit_at + 1.hour,
        atya_unit_status: "acceptable",
        atya_requires_donor_deferral: false,
        nat_requires_donor_deferral: false,
        nat_unit_status_completed_at: visit_at + 1.hour,
        nat_review_completed_at: visit_at + 1.hour,
        spe_physician_requires_donor_hold: false
      )
    end
  end

  donor
end

def set_up_machine(facility: Facility.find(2), prefix: "A")
  ActiveRecord::Base.transaction do
    apheresis_machine = facility.apheresis_machines.free.where(serial_number: "#{prefix}BC-NICO").first_or_create!

    ApheresisMachineSetup::NewSetup.new(
      employee: Employee.first, #Account.find_by!(email: "nicholas@buoysoftware.com").employee,
      facility:,
      apheresis_machine:, # need to set quantity to 999 or some high number
      anticoagulant: facility
        .soft_goods
        .anticoagulant
        .where(lot_number: "ag123")
        .first_or_create!.tap { |x|
          x.update! quantity: 999
        }.lot_number,
      bottle: facility
        .soft_goods
        .bottle
        .where(lot_number: "bt123")
        .first_or_create!.tap { |x|
          x.update! quantity: 999
        }.lot_number,
      bowl: facility
        .soft_goods
        .bowl
        .where(lot_number: "bw123")
        .first_or_create!.tap { |x|
          x.update! quantity: 999
        }.lot_number,
      harness: facility
        .soft_goods
        .harness
        .where(lot_number: "hn123")
        .first_or_create!.tap { |x|
          x.update! quantity: 999
        }.lot_number,
      saline: facility
        .soft_goods
        .saline
        .where(lot_number: "sl123")
        .first_or_create!.tap { |x|
          x.update! quantity: 999
        }.lot_number,
      soft_goods_acceptable: true
    ).save
  end

  apheresis_machine.serial_number
end
alias :sum :set_up_machine

def undo_phlebotomy(queued_donor_id=19)
  ActiveRecord::Base.transaction do
    queued_donor = QueuedDonor.find(queued_donor_id)
    donation = queued_donor.visit
    set_up_paper_trail facility: donation.facility
    donation.update! phlebotomy_started_at: nil
    donation.phlebotomy.reset! comment: "reset from console"
    donation.phlebotomy_attempts.destroy_all
  end
end
alias :undop :undo_phlebotomy

# ===================== PaperTrail (with donor ID) =====================
donor_id = 84408
donor = Donor.find(donor_id)
facility = donor.facility
location = facility.center_location
employee_first_name = "Kirstin"
employee_last_name = "Clawson"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}

# ===================== PaperTrail (with facility name) =====================
facility_name = "Crossville"
facility = Facility.find_by(name: facility_name)

if facility.dhq_enabled?
  puts "======= DHQ IS ENABLED ======="
else
  location = facility.center_location
  employee_first_name = "Casey"
  employee_last_name = "Wilbanks"
  employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
  engineer = Account.find_by(email: "nicholas@buoysoftware.com")
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }
end

# ===================== Common objects =====================
donor_id = 15403
donor = facility.donors.find(donor_id)
donation_id = 83586
donation = donor.visits.find(donation_id)
screening = donation.screening
queued_donor = donation.queued_donor
setup = donation.setup
unit = donation.unit
sample = unit&.sample
sample_review = donation&.sample_review

# TEMP
donation.sample_review.attributes.slice "id", "spe_physician_requires_recollection"
donation.sample_review.update! spe_physician_requires_recollection: false

# ===================== Switch facilities =====================
facility_name = "Tahlequah"
facility = Facility.find_by(name: facility_name)
location = facility.center_location
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}

# ===================== Test results with paper trail =====================
donor_id = 112448
donor = Donor.find(donor_id)
facility = donor.facility
location = facility.center_location
employee_first_name = "Eva"
employee_last_name = "Anderson"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}
ucn = "CN1000000899"
unit = donor.units.find_by(control_number: ucn)
sample = unit.sample
test_results = TestResult
	.joins(:test_result_scores)
	.where(test_result_scores: { unit_control_number: ucn })
	.uniq
test_results.each do |test_result|
  TestResultScore::Pivot.new(test_result: test_result).save
end


ucns = %w[
  SV1000006145
  TO1000008678
  TO1000008650
  EI1000003316
  EI1000003898
  EI1000002939
  GV1000008343
  EI1000002582
  AR1000035649
]
ucns.each do |ucn|
  test_results = TestResult
    .joins(:test_result_scores)
    .where(test_result_scores: { unit_control_number: ucn })
    .uniq
  test_results.each do |test_result|
    TestResultScore::Pivot.new(test_result: test_result).save
  end
end

# ===================== Nullify phone number =====================
phone_number = "+19033367888"
account = Account.find_by(phone_number:)
donor = account.donor
facility = donor.facility
location = facility.center_location
employee_first_name = "Eva"
employee_last_name = "Anderson"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
engineer = Account.find_by(email: "nicholas@buoysoftware.com")
PaperTrail.request.whodunnit = engineer.id
PaperTrail.request.controller_info = {
  employee_id: employee.id,
  facility_id: facility.id,
  location_id: location.id,
}
account.attributes.slice("id", "phone_number")
account.update! phone_number: nil
account.reload.attributes.slice("id", "phone_number")


# ===================== UPDATE CASE LABEL =====================
shipment = Shipment.find(276) # from URL Luke pasted in Slack: https://app.buoysoftware.com/locations/11/casing/shipments/276/bill-of-lading
plasma_case = shipment.plasma_cases.first # there is only one
plasma_case.update! label_id: "100014211899"


# ===================== Update case label by old label =====================
plasma_case = facility.plasma_cases.find_by(label_id: "100072200108")
plasma_case.attributes.slice "id", "label_id"
plasma_case.update! label_id: "100072200087"
plasma_case.reload.attributes.slice "id", "label_id"


# ===================== Incorrect screening equipment =====================
screening.screening_equipment.map { |x| [x.equipment.serial_number, x.equipment.equipment_type] }

# incorrect_weight_scale = Equipment.find_by(serial_number: "ws155800012")
# correct_weight_scale = Equipment.find_by(serial_number: "ws15580012")

incorrect_vital_signs_monitor = Equipment.find_by(serial_number: "vs155800013")
correct_vital_signs_monitor = Equipment.find_by(serial_number: "vs15580013")

incorrect_hematastat = Equipment.find_by(serial_number: "hs155800011")
correct_hematastat = Equipment.find_by(serial_number: "hs15580011")

# weight_scale_association = screening.screening_equipment.find_by(equipment: incorrect_weight_scale)
# weight_scale_association.update! equipment: correct_weight_scale

vital_signs_association = screening.screening_equipment.find_by(equipment: incorrect_vital_signs_monitor)
vital_signs_association.update! equipment: correct_vital_signs_monitor

hematastat_association = screening.screening_equipment.find_by(equipment: incorrect_hematastat)
hematastat_association.update! equipment: correct_hematastat

screening.reload.screening_equipment.map { |x| [x.equipment.serial_number, x.equipment.equipment_type] }


# ===================== UPDATE CONSIGNEE ===========================
def set_papertrail(facility)
  location = facility.center_location
  employee_first_name = "Ticket author first name"
  employee_last_name = "Ticket author last name"
  employee = Employee.find_by(first_name: employee_first_name, last_name: employee_last_name)
  engineer = Account.find_by(email: "YOUR_EMAIL@buoysoftware.com")
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }
end

def update_shipment(shipment)
  facility = shipment.consignee_facility.facility
  set_papertrail facility

  old_consignee_name = "BioLife Plasma Services, LLC – c/o RXCrossroads, Erlanger, KY"
  new_consignee_name = "BioLife Plasma Services - Plasma Operations"

  old_consignee = Consignee.find_by(name: old_consignee_name)
  new_consignee = Consignee.find_by(name: new_consignee_name)
  old_consignee_facilities = old_consignee
    .consignee_facilities
    .map { [_1.facility.name, _1.id] }
    .to_h
  new_consignee_facilities = new_consignee
    .consignee_facilities
    .map { [_1.facility.name, _1.id] }
    .to_h

  return_hash = {}
  return_hash[:before] = shipment
    .consignee_facility
    .attributes
    .slice("id", "consignee_id", "product_types", "status")
    .tap { _1["consignee"] = shipment.consignee.name }

  shipment.update! consignee_facility_id: new_consignee_facilities[facility.name]

  return_hash[:after] = shipment
    .reload
    .consignee_facility
    .attributes
    .slice("id", "consignee_id", "product_types", "status")
    .tap { _1["consignee"] = shipment.consignee.name }

  return_hash
end

# This will vary depending on ticket of course:
kingsville_shipment = Shipment.find(527)
el_dorado_shipment = Shipment.find(468)
brownwood_shipment = Shipment.find(517)

update_shipment(kingsville_shipment)
update_shipment(el_dorado_shipment)
update_shipment(brownwood_shipment)

# ===================== UPDATE TEST NUMBER =====================
def set_papertrail(facility: nil, donation_id: nil)
  raise "NOPE" unless [facility, donation_id].any?(&:present?)
  facility = facility || Donation.find(donation_id).facility
  location = facility.center_location
  employee_first_name = "Luke"
  employee_last_name = "Winter"
  employee = Employee.find_by(first_name: employee_first_name, last_name: employee_last_name)
  engineer = Account.find_by(email: "nicholas@buoysoftware.com")
  PaperTrail.request.whodunnit = engineer.id
  PaperTrail.request.controller_info = {
    employee_id: employee.id,
    facility_id: facility.id,
    location_id: location.id,
  }
end

def update_test_number(data:)
  sample = Sample.find_sole_by(data[:type] => data[:wrong])
  facility = sample&.visit&.facility
  if facility
    set_papertrail(facility:)
    return_hash = { sample_before: sample.attributes.slice("id", data[:type].to_s, donation_id: sample.visit.id) }
    sample.update! data[:type] => data[:right]
    return_hash[:sample_after] = sample.reload.attributes.slice("id", data[:type].to_s, donation_id: sample.visit.id)
    return_hash
  else
    raise "NOPE"
  end
end
data = { type: :vmt, wrong: "VS1000004410N", right: "VS1000004410V" }
update_test_number data:

# ===================== END DEFERRAL =====================
donor_id = 68769
donor = Donor.find(donor_id)
deferral_id = 46254
deferral = donor.donor_deferrals.find(deferral_id)
deferral_end_date = Date.parse("2024-02-23")
deferral_comment = "Deferral ended per change request #1511 (prior to deletion)"
employee = Employee.find_by!(email: "nicholas@buoysoftware.com")
facility = donor.facility
employee_first_name = "Luke"
employee_last_name = "Winter"
employee = facility.employees.find_by(first_name: employee_first_name, last_name: employee_last_name)
set_papertrail(facility:)
return_hash = { before: { donor_id: donor.id, deferral_id: deferral.id, donor_deferred: donor.deferred? } }
deferral.update!(
  ends_on: deferral_end_date,
	terminator: employee,
	comment: deferral_comment
  )
DonorDeferral::DeferralLiftedMessage.publish(deferral)
return_hash[:after] = { donor_id: donor.id, deferral_id: deferral.id, donor_deferred: donor.reload.deferred? }
return_hash


# ====================== UPDATE WEIGHT ============================
weight = 247
weight_acceptable = true
Screening::Weight.new(queued_donor:, actor: employee, weight:, weight_acceptable:).save
# This updates screening and donor records
# Check that screening has correct volume tier, Screening::Weight does not update that:
screening.volume_tier == VolumeTier.by_weight(weight).tier_name
# If not, then update screening:
screening.update! volume_tier: VolumeTier.by_weight(weight).tier_name
