require 'pry'

class PacmanListService
  private
  attr_reader :final_list, :group_list, :package_list

  public

  def self.run
    new.run
  end

  def initialize
    @groups = group_query_lines.map(&:split).map(&:first).uniq
    @group_packages = group_query_lines.map(&:split).map(&:last).uniq

    @all_packages = all_query_lines.map(&:split).map(&:first)
  end

  def run
    puts "Groups:"

    (@groups - @group_packages).each { |group| puts "  #{group}" }

    puts "\nIndividual Packages:"

    (@all_packages - @group_packages).each { |package| puts "  #{package}" }
  end

  private

  def all_query_lines
    @_all_query_lines ||= `pacman -Qe`.split("\n")
  end

  def group_query_lines
    @_group_query_lines ||= `pacman -Qg`.split("\n")
  end
end

PacmanListService.run
