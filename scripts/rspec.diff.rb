#!/usr/bin/env ruby

files_in_diff = `git diff --name-only main`.split

spec_files_in_diff = files_in_diff.select do |f|
  f.match(/spec\/.*_spec\.rb\Z/) && File.exist?(f)
end

Process.kill('HUP', Process.pid) if spec_files_in_diff.none?

unless ARGV[0] == "--dryrun"
  puts spec_files_in_diff.join(' ')
end
