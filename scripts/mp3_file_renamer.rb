require 'yaml'

class RenameTrack
  RIPPED_CDS_PATH = '/home/nmertaugh/Music/Ripped CDs'

  def self.run(cd_directory, ripped_cds_path = nil, prepend: false, extension: "flac")
    new(
      cd_directory,
      ripped_cds_path,
      prepend: prepend,
      extension: extension
    ).rename
  end

  def initialize(cd_directory, ripped_cds_path, prepend:, extension:)
    # Expects a file named track_names.yml to be in same  directory as MP3s
    @cd_directory = cd_directory
    @ripped_cds_path = ripped_cds_path || RIPPED_CDS_PATH
    @extension = extension

    unless prepend
      @index_to_disc_number_and_track_name_map = index_to_disc_number_and_track_name_map if multidisc?
    end
  end

  def prepend_track_number
    Dir["#{@ripped_cds_path}/#{@cd_directory}/*.#{@extension}"]
      .map { |file_name| File.new(file_name) }
      .sort { |a, b| a.birthtime <=> b.birthtime }
      .each_with_index do |file_path, index|
        file_parts = file_path.path.split('/')
        file_name = "#{(index + 1).to_s.rjust(2, '0')} - #{file_parts.pop}"
        desired_file_name = file_parts.push(file_name).join('/')
        puts "Renaming '#{file_path.path}' to '#{desired_file_name}'"
        File.rename file_path.path, desired_file_name
      end
  end

  def rename
    Dir["#{@ripped_cds_path}/#{@cd_directory}/*.#{@extension}"]
      .map { |file_name| File.new(file_name) }
      .sort { |a, b| a.birthtime <=> b.birthtime }
      .each_with_index do |file_path, index|
        desired_file_name = desired_file_path_for(index)
        puts "Renaming '#{file_path}' to '#{desired_file_name}'"
        File.rename file_path, desired_file_name
      end
  end

  private

  def desired_file_names
    @desired_file_names ||= begin
      path_to_yaml_file = "#{@ripped_cds_path}/#{@cd_directory}/track_names.yml"
      YAML.load File.read(path_to_yaml_file)
    end
  end

  def desired_file_path_for(index)
    if multidisc?
      disc_number, track_number, file_name = @index_to_disc_number_and_track_name_map[index]
    else
      disc_number = nil
      track_number = index + 1
      file_name = desired_file_names[index]
    end

    track_number = track_number.to_s.rjust(2, '0')
    disc_number = "#{disc_number.to_s.rjust(2, '0')} " unless disc_number.nil?
    puts "#{@ripped_cds_path}/#{@cd_directory}/#{disc_number}#{track_number} - #{file_name.gsub('/', ', ')}.#{@extension}"
    "#{@ripped_cds_path}/#{@cd_directory}/#{disc_number}#{track_number} - #{file_name.gsub('/', ', ')}.#{@extension}"
  end

  def multidisc?
    desired_file_names.is_a?(Hash)
  end

  def index_to_disc_number_and_track_name_map
    index = 0

    desired_file_names.each_with_object({}) do |(disc_num, track_names), hash|
      track_number = 1

      track_names.each do |track_name|
        hash[index] = [disc_num, track_number, track_name]
        index += 1
        track_number += 1
      end
    end
  end
end
