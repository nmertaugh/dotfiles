#!/usr/bin/env ruby

files_in_diff = `git diff --name-only main`.split

puts files_in_diff.join(' ')
