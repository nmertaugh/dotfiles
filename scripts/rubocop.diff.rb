#!/usr/bin/env ruby
# frozen_string_literal: true

files_in_diff = `git diff --name-only main`.split

ruby_files_in_diff = files_in_diff.select do |f|
  f.match(/\.(rb|rake)\Z/) &&
    File.exist?(f) &&
    !f.match(/schema\.rb\Z/)
end

Process.kill('HUP', Process.pid) if ruby_files_in_diff.none?

unless ARGV[0] == "--dryrun"
  puts ruby_files_in_diff.join(' ')
end
