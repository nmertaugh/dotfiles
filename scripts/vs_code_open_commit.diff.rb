#!/usr/bin/env ruby

files_in_diff = `git diff-tree --no-commit-id --name-only #{ARGV[0]} -r`.split

puts files_in_diff.join(' ')
