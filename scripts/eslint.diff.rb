#!/usr/bin/env ruby

files_in_diff = `git diff --name-only main...`.split

js_files_in_diff = files_in_diff.select do |f|
  f.match(/\.(ts|tsx|graphql)\Z/) &&
    File.exist?(f) &&
    !f.match(/schema\.graphql\Z/) &&
    !f.match(/__generated__\/.*\Z/) &&
    !f.match(/generated.*\.ts\Z/)
end

puts js_files_in_diff.join(' ')
