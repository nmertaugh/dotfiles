#!/bin/bash

add_frozen_string_literal_comment () {
  STR='# frozen_string_literal: true'
  REGEX="^$STR"

  if ! [[ $(head $1) =~ $REGEX ]]; then
    sed -i "1s;^;$STR\\n\\n;" $1
  fi
}

bungle () {
  bundle $1 --gemfile /home/nmertaugh/.bundle/Gemfile;
}

check_checksums () {
  IFS= read -r piped_input
  echo $piped_input | cut -f1 -d' ' | is_equal $1
}

eslint () {
  FILES_IN_DIFF=()
  readarray -t FILES_IN_DIFF < <( git diff --name-only $1... )

  ./node_modules/.bin/eslint --fix ${FILES_IN_DIFF}
}

eslint2 () {
  FILES_IN_DIFF=()
  readarray -t FILES_IN_DIFF < <( git diff --name-only $1... )

  for ((i=0; i<${#FILES_IN_DIFF[*]}; i++));
  do
    echo "Analyzing ${FILES_IN_DIFF[i]}"
    ./node_modules/.bin/eslint --fix ${FILES_IN_DIFF}
  done
}

gitbase () {
  git rebase -i HEAD~$1;
}

gelete () {
  CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  BRANCHES=()
  readarray -t BRANCHES < <( git branch )

  printf "Which branch would you like to delete locally?\n\n"

  for ((i=0; i<${#BRANCHES[*]}; i++));
  do
    printf "  ${i}: ${BRANCHES[i]}\n"
  done

  printf "\n"

  read CHOICE

  if [[ ${BRANCHES[$CHOICE]/* /} = $CURRENT_BRANCH ]]; then
    printf "You can't delete the branch you currently have checked out!\n\n"
  else
    printf "CONFIRM: Delete ${BRANCHES[$CHOICE]/* /} locally? [N|y]"

    read -s -n 1 CONFIRM

    if [[ $CONFIRM = "y" ]]; then
      printf "\n\nDeleting ${BRANCHES[$CHOICE]/* /} locally..."

      git branch -D "${BRANCHES[$CHOICE]/* /}"

      printf "Would you like to delete ${BRANCHES[$CHOICE]/* /} from ORIGIN as well? [N|y]"

      read -s -n 1 CONFIRM

      if [[ $CONFIRM = "y" ]]; then
        printf "\n\nDeleting ${BRANCHES[$CHOICE]/* /} from ORIGIN..."

        git push --delete origin "${BRANCHES[$CHOICE]/* /}"
      fi
    fi
  fi
}

gerge () {
  CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  BRANCHES=()
  readarray -t BRANCHES < <( git branch )

  printf "Which branch would you like to merge into ${CURRENT_BRANCH}?\n\n"

  for ((i=0; i<${#BRANCHES[*]}; i++));
  do
    printf "  ${i}: ${BRANCHES[i]}\n"
  done

  printf "\n"

  read CHOICE

  printf "Merge ${BRANCHES[$CHOICE]/* /} into ${CURRENT_BRANCH}? [y|N] "

  read -s -n 1 CONFIRM

  if [[ $CONFIRM = "y" ]]; then
    printf "\n\nMerging ${BRANCHES[$CHOICE]/* /} into ${CURRENT_BRANCH}\n\n"

    git merge "${BRANCHES[$CHOICE]/* /}"
  fi
}

geckout () {
  BRANCHES=()
  readarray -t BRANCHES < <( git branch )

  printf "Which branch would you like to check out?\n\n"

  for ((i=0; i<${#BRANCHES[*]}; i++));
  do
    printf "  ${i}: ${BRANCHES[i]}\n"
  done

  printf "\n"

  read CHOICE

  git co "${BRANCHES[$CHOICE]/* /}"
}

git_open () {
  FILES=$(git files)

  for f in $FILES
  do
  	a $f
  done
}

git_origin_push () {
  CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

  if [[ $CURRENT_BRANCH != "master" ]]; then
    printf "\nWould you like to FORCE push? [N|y] "

    read -s -n 1 FORCE

    if [[ $FORCE == "y" ]]; then
      printf "\nForce push ${CURRENT_BRANCH} up to ORIGIN? [N|y] "
    else
      printf "\nPush ${CURRENT_BRANCH} up to ORIGIN? [N|y] "
    fi
  else
    printf "\nPush ${CURRENT_BRANCH} up to ORIGIN? [N|y] "
  fi

  read -s -n 1 CONFIRM

  if [[ $CONFIRM = "y" ]]; then
    printf "\n\nPushing ${CURRENT_BRANCH} to ORIGIN...\n\n"

    if [[ $CURRENT_BRANCH == "master" ]] || [[ $FORCE != "y" ]]; then
      git push origin "${CURRENT_BRANCH}"
    else
      git push -f origin "${CURRENT_BRANCH}"
    fi
  fi
}

gitclean () {
  find . -type f -name "* conflicted copy*" -exec rm -f {} \;
  awk '!/conflicted/' .git/packed-refs > temp && mv temp .git/packed-refs;
}

git_delete () {
  git push -d origin $1;
  git br -D $1;
}

is_equal () {
  IFS= read -r piped_input
  [[ $1 == $piped_input ]] && echo equal || echo not-equal
}

kc () {
  printf "Which context would you like to work with?\n\n"

  readarray -t CONTEXTS < <( kubectl config get-contexts -o name )

  for ((i=0; i<${#CONTEXTS[*]}; i++));
  do
    printf "  $(( i + 1 )): ${CONTEXTS[i]}\n"
  done

  printf "\nSelect context: "

  read CONTEXT_CHOICE

  CONTEXT_INDEX=$(( CONTEXT_CHOICE - 1 ))

  printf "\n"

  kubectl config use-context ${CONTEXTS[CONTEXT_INDEX]}

  printf "\nFilter pods in ${CONTEXTS[CONTEXT_INDEX]} by: (ALL|filter)\n\n"

  read FILTER

  if [[ -z $FILTER ]]; then
    printf "Getting list of all pods...\n\n"
    readarray -t PODS < <( kubectl get pods | tail -n +2 | cut -f1 -d ' ' - )
  else
    printf "Getting pods for filter: ${FILTER}...\n\n"
    readarray -t PODS < <( kubectl get pods | grep $FILTER | cut -f1 -d ' ' - )
  fi

  for ((i=0; i<${#PODS[*]}; i++));
  do
    printf "  $(( i + 1 )): ${PODS[i]}\n"
  done

  printf "\nSelect pod: "

  read POD_CHOICE
  POD_INDEX=$(( POD_CHOICE - 1 ))

  read -ra POD <<< "${PODS[$POD_INDEX]}"

  printf "\n\nWhat operation would you like to run on ${POD}?\n\n"
  echo -e  "  1. Run Rails console"
  echo -e  "  2. Run Ruby console"
  echo -e  "  3. Run shell"
  echo -e  "  4. Get info"

  printf "\nSelect operation: "

  read OPERATION

  if [[ $OPERATION == "1" ]]; then
    printf "\nConnecting to Rails console on ${POD}...\n\n"
    kubectl exec -it ${POD} -- bundle exec rails console
  elif [[ $OPERATION == "2" ]]; then
    printf "\nConnecting to Ruby console on ${POD}...\n\n"
    kubectl exec -it ${POD} c ruby-container -- bin/console
  elif [[ $OPERATION == "3" ]]; then
    printf "\nConnecting to shell on ${POD}...\n\n"
    kubectl exec -it ${POD} c ruby-container -- /bin/sh
  elif [[ $OPERATION == "4" ]]; then
    kubectl describe pod ${POD}
  fi
}

# KUBE_EDITOR="atom -w" kubectl edit secrets sinatra-service-template-secrets-staging
# kubectl delete secret sinatra-service-template-secrets-staging
# kubectl create secret generic sinatra-service-template-secrets-staging --from-literal=NEW_RELIC_LICENSE_KEY=placeholder-value-please-replace --from-literal=RABBIT_MQ_PASSWORD=placeholder-value-please-replace --from-literal=ROLLBAR_ACCESS_TOKEN=placeholder-value-please-replace

memory () {
  free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%)\n", $3,$2,$3*100/$2 }';
  df -h | awk '$NF=="/"{printf "Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}';
  top -bn1 | grep load | awk '{printf "CPU Load: %.2f\n", $(NF-2)}' ;
}

myip () {
  ip route get 1.2.3.4 | awk '{print $7}';
}

rtorrent_stop () {
  pid=$(ps -A | grep 'rtorrent' | awk '{print $1}')
  sudo kill -9 $pid
}

prepend_disc_number () {
  for file in *mp3
  do
    mv "$file" "$1 $file"
  done
  ls
}

psql_run () {
  database='template1'

  if [[ $2 ]]; then
    database=$2
  fi

  psql -c $1 -d $database
}

rename_file_extensions () {
  for file in *.css.sass; do
    mv "$file" "$(basename "$file" .css.sass).sass"
  done
}

resize_images () {
  for i in *.{jpg,JPG,png,PNG}; do
    printf "Resizing $i\n"
    convert "$i" -resize 40% "$i"
  done
}

rip_ps1 () {
  # Rips to /dev/sr1, which is currently the DVD drive
  mkdir "$1"
  cdrdao read-cd --read-raw --datafile "$1/$1.bin" --device /dev/sr1 --driver generic-mmc-raw "$1/$1.cue"
}

sync_black () {
  sync_music_library '32GB BLACK' 'Black Metal'
  sync_music_library '32GB BLACK' 'Breakbeat'
  sync_music_library '32GB BLACK' 'Death Metal'
  sync_music_library '32GB BLACK' 'Doom Metal'
  sync_music_library '32GB BLACK' 'Glam Rock'
  sync_music_library '32GB BLACK' 'Thrash Metal'
  sync_music_library '32GB BLACK' 'Video Games'
}

sync_blue () {
  sync_music_library '32GB BLUE' 'Podcasts';
}

sync_green () {
  sync_music_library '32GB GREEN' 'Andean'
  sync_music_library '32GB GREEN' 'Bluegrass'
  sync_music_library '32GB GREEN' 'Country'
  sync_music_library '32GB GREEN' 'Flamenco'
  sync_music_library '32GB GREEN' 'Jazz'
}

sync_pink () {
  sync_music_library '32GB PINK' 'Audio Books'
  sync_music_library '32GB PINK' 'Movie Rips'
}

sync_white () {
  sync_music_library '32GB WHITE' 'Classical'
  sync_music_library '32GB WHITE' 'Christmas'
}

sync_yellow () {
  sync_music_library '32GB YELLOW' 'Soundtracks'
}

sync_music_library () {
  rsync -avP --ignore-existing --delete --delete-excluded \
    --exclude-from=/home/nmertaugh/Development/Active/personal/dotfiles/rsync_exclude_from.txt \
    "/home/nmertaugh/Music/Music Library/$2" "/run/media/nmertaugh/$1/Music Library"
}

up () { # grep through bash Hx for line beginning w/ arg
  history | grep '^[[:space:]]*[0-9]*[[:space:]]*'$1;
}

xmlint () { # add line breaks and indenting to XML file
  xmllint --format $1 > $1.formatted
  mv $1 $1.orig
  mv $1.formatted $1
}

xerxes () {
  CYAN='\033[0;36m'
  NC='\033[0m' # No Color

  if [ -z "${1+x}" ]
  then
    echo -e  "Which repo would you like to work on?\n"
    echo -e  "    1.  Personal:        Dotfiles"
    echo -e  "    2.  Personal:        Vocabulous"
    echo -e  "    3.  Personal:        Learn Go"
    echo -e  "    4.  Personal:        Learn React"
    echo -e  "    5.  Personal:        Algorithm Practice"
    echo -e  "    6.  Personal:        Portsmouth Rain"
    echo -e  "    7.  Personal:        Notes"
    echo -e  "    8.  Personal:        Soundtrack Ripper"
    echo -e  "    9.  Personal:        Backblaze Backup Script (no git)"
    echo -e  "   ${CYAN}10.  Buoy:            Buoy Rails${NC}"
    echo -e  "   ${CYAN}11.  Buoy:            DevOps${NC}"
    echo -e  "   ${CYAN}12.  Buoy:            Wharf${NC}"
    echo -e  "   ${CYAN}13.  Buoy:            Guides${NC}"
    echo -e  "   ${CYAN}14.  Buoy:            Anchor UI${NC}"
    echo -e  "   ${CYAN}15.  Buoy:            Anchor View Components${NC}"
    echo -e  "   ${CYAN}16.  Buoy:            Tide${NC}"
    echo -e  "   ${CYAN}17.  Buoy:            Infirmary${NC}"

    read choice
  else
    choice="$1"
  fi

  DEVELOPMENT_ROOT='/home/nmertaugh/Development'
  PERSONAL_PATH="${DEVELOPMENT_ROOT}/personal"
  BUOY_PATH="${DEVELOPMENT_ROOT}/buoy"
  REPOS=(
    "${PERSONAL_PATH}/dotfiles"
    "${PERSONAL_PATH}/vocabulous"
    "${PERSONAL_PATH}/learn_go"
    "${PERSONAL_PATH}/learn_react"
    "${PERSONAL_PATH}/algorithm_practice"
    "${PERSONAL_PATH}/portsmouth_rain"
    "${PERSONAL_PATH}/notes"
    "${PERSONAL_PATH}/soundtrack_ripper"
    "${PERSONAL_PATH}/backup_strategy"
    "${BUOY_PATH}/BuoyRails"
    "${BUOY_PATH}/DevOps"
    "${BUOY_PATH}/Wharf"
    "${BUOY_PATH}/Guides"
    "${BUOY_PATH}/AnchorUi"
    "${BUOY_PATH}/anchor_view_components"
    "${BUOY_PATH}/Tide"
    "${BUOY_PATH}/Infirmary"
  )

  cd "${REPOS["$choice" - 1]}"
}

open_sha () {
  a $(/home/nmertaugh/Development/personal/dotfiles/scripts/vs_code_open_commit.diff.rb $1)
}
