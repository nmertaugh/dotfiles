require_relative "./colorize"

module FahrenheitCelcius
  extend Colorize
  module_function

  def print
    puts <<-HEREDOC

    #{colorize('Fahrenheit', :light_blue)} to #{colorize('Celcius', :red)}:

      #{colorize('C', :red)} = (#{colorize('F', :light_blue)} - 32) * 5/9


    #{colorize('Celcius', :red)} to #{colorize('Fahrenheit', :light_blue)}:

      #{colorize('F', :light_blue)} = #{colorize('C', :red)} * 9/5 + 32

    HEREDOC
  end
end

FahrenheitCelcius.print
