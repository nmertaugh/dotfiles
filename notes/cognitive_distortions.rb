require_relative "./colorize"

module CognitiveDistortions
  extend Colorize
  module_function

  def print
    puts <<-HEREDOC

    #{colorize('ALWAYS BEING RIGHT:')}

      Being wrong is unthinkable. This cognitive distortion is characterized
      by actively trying to prove one's actions or thoughts to be correct,
      and sometimes prioritizing self-interest over the feelings of another
      person.


    #{colorize('BLAMING:')}

      The opposite of personalization; holding other people responsible for
      the harm they cause, and especially for their intentional or negligent
      infliction of emotional distress.


    #{colorize('DISQUALIFYING THE POSITIVE:')}

      Discounting positive events.


    #{colorize('EMOTIONAL REASONING:')}

      Presuming that negative feelings expose the true nature of things and
      experiencing reality as a reflection of emotionally linked thoughts.
      Thinking something is true, solely based on a feeling.


    #{colorize('FALLACY OF CHANGE:')}

      Relying on social control to obtain cooperative actions from another
      person.


    #{colorize('FALLACY OF FAIRNESS:')}

      This is the belief that life should be fair and produces upset or angry
      emotions when life is perceived as failing to be fair and breaking rules
      to even the playing field that leads to long term ramifications.


    #{colorize('MENTAL FILTERING:')}

      Focusing entirely on negative elements of a situation to the exclusion
      of the positive. Also, the brain's tendency to filter information that
      does not conform to already-held beliefs.


    #{colorize('JUMPING TO CONCLUSIONS:')}

      Reaching preliminary conclusions (usually negative) with little (if any)
      evidence. Two specific subtypes are identified:

          #{colorize('Mind reading:', :yellow)} Inferring a person's possible or probable (usually
          negative) thoughts from his or her behavior and nonverbal communication;
          taking precautions against the worst suspected case without asking the
          person.

          #{colorize('Fortune-telling:', :yellow)} predicting outcomes (usually negative) of events.


    #{colorize('LABELING AND MISLABELING:')}

      A form of overgeneralization; attributing a person's actions to his or
      her character instead of to an attribute. Rather than assuming the
      behavior to be accidental or otherwise extrinsic, one assigns a label
      to someone or something that is based on the inferred character of
      that person or thing.


    #{colorize('MAGNIFICATION AND MINIMIZATION:')}

      Giving proportionally greater weight to a perceived failure, weakness
      or threat, or lesser weight to a perceived success, strength or opportunity,
      so that the weight differs from that assigned by others, such as "making a
      mountain out of a molehill". In depressed clients, often the positive
      characteristics of other people are exaggerated and their negative
      characteristics are understated.

          #{colorize('Catastrophizing:', :yellow)} Giving greater weight to the worst possible outcome,
          however unlikely, or experiencing a situation as unbearable or impossible
          when it is just uncomfortable.


    #{colorize('OVERGENERALIZING:')}

      Making hasty generalizations from insufficient evidence. Drawing a very
      broad conclusion from a single incident or a single piece of evidence.
      Even if something bad happens only once, it is expected to happen over
      and over again.


    #{colorize('PERSONALIZING:')}

      Attributing personal responsibility, including the resulting praise or
      blame, to events over which the person has no control.


    #{colorize('MAKING "MUST" OR "SHOULD" STATEMENTS:')}

      Making 'must' or should' statements was included by Albert Ellis in his
      rational emotive behavior therapy, an early form of CBT; he termed it
      "musturbation". Michael C. Graham called it "expecting the world to be
      different than it is". It can be seen as demanding particular
      achievements or behaviours regardless of the realistic circumstances of
      the situation.


    #{colorize('SPLITTING (ALL-OR-NOTHING THINKING, BLACK-OR-WHITE THINKING, DICHOTOMOUS REASONING):')}

      Evaluating the self, as well as events in life in extreme terms. It's
      either all good or all bad, either black or white, nothing in between.
      Even small imperfections seem incredibly dangerous and painful. Splitting
      involves using terms like "always", "every" or "never" when they are
      false and misleading.

    HEREDOC
  end
end

CognitiveDistortions.print
