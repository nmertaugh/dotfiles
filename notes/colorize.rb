module Colorize
  def colorize(string, color = :green)
    color_code =
      case color
      when :blue
        34
      when :green
        32
      when :gray
        30
      when :light_blue
        36
      when :purple
        35
      when :red
        31
      when :yellow
        33
      end

    "\e[#{color_code}m#{string}\e[0m"
  end
end
