require_relative "./colorize"

module ModdingProjects
  extend Colorize
  module_function

  def print
    puts <<-HEREDOC

    #{colorize("====================================", :light_blue)}
    #{colorize("SCART consoles", :light_blue)} (#{colorize('red', :red)} needs cables):
    #{colorize("====================================", :light_blue)}

      NES ---------------------
      SNES                    |
      N64                     |
      GameCube                | Switch 1
      PS1                     |
      PS2                     |
      Sega Master System ------
      Sega Genesis ------------
      #{colorize("Sega Saturn", :red)}             |
      #{colorize("Sega Dreamcast", :red)}          |
      #{colorize("Turbo Grafx 16", :red)}          | Switch 2
      #{colorize("3DO", :red)}                     |
      #{colorize("NeoGeo Open MVS", :red)}         |
      #{colorize("Xbox Original", :red)} -----------
      #{colorize("Atari 2600", :red)} --------------
      #{colorize("Atari 5200", :red)}              |
      #{colorize("Atari 7600", :red)}              | Switch 3?
      #{colorize("Atari Jaguar", :red)}            |
      #{colorize("Colecovision", :red)} ------------

                            Cables: $???
                            gScart: $???
                            #{colorize("Total:  $600", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("Sega Saturn", :light_blue)}
    #{colorize("====================================", :light_blue)}

      Fenrir ODE:           $140
      SCART Cable:          $50
      Battery Mod:          $10???
      Cap Kit (main board): $11
      Cap Kit (PSU):        $6

                            #{colorize("Total:  $???", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("Sega Dreamcast", :light_blue)}
    #{colorize("====================================", :light_blue)}

      SCART Cable:          $50
      V0 -> V1 Mod:         $10???
      Cap Kit (main board): $11
      Cap Kit (PSU):        $6

                            #{colorize("Total:  $???", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("Turbo Grafx 16", :light_blue)}
    #{colorize("====================================", :light_blue)}

      Turbo Everdrive Pro: $200
      SCART Cable:         $50
      Cap Kit:             $5.50

                            #{colorize("Total:  $???", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("3DO", :light_blue)}
    #{colorize("====================================", :light_blue)}

      OEM Controller:           $50???
      3DO FZ-1 ODE:             $250
      SCART Cable:              $50
      3DO BT:                   $50
      Humble Bazooka ODE Mount: $15
      Micro SD Extender:        $10
      CD Drive Tray Clips:      $5
      Noctua Fan Cable:         $5
      Noctua Fan:               $15
      Cap Kit:                  $15

                            #{colorize("Total:  $???", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("Neo Geo Open MVS", :light_blue)}
    #{colorize("====================================", :light_blue)}

      HDMI Kit:             $225???
      OMVS Case:            $100
      SCART Cable:          $50
      OMVS Kit:             $120???
      8BitDo Controller x2: $60
      NeoBT *2:             $100
      Cap Kit:              $9

                            #{colorize("Total:  $???", :green)}

    #{colorize("====================================", :light_blue)}
    #{colorize("Xbox Original", :light_blue)}
    #{colorize("====================================", :light_blue)}

      Shell (+ shipping): $160
      RGB Mod?:
      HDMI Mod:
      Project Stella:
      SCART Cable?:
      OEM Controller x2:  $???

                            #{colorize("Total:  $???", :green)}
    HEREDOC
  end
end

ModdingProjects.print
