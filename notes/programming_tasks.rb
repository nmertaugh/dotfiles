require_relative "./colorize"

module ProgrammingTasks
  extend Colorize
  module_function

  def print
    puts <<-HEREDOC

    #{colorize('Pacman Cheat Sheet:', :red)}

    - Search for info on packages:

    #{colorize('  sudo pacman -Ss pkg_name', :light_blue)}

    - Look at all installed packages (alias command):

    #{colorize('  pacman.ls', :light_blue)}


    #{colorize('Command Line Tools:', :red)}

    - Search for file in entire system, case insensitive:

    #{colorize('  find / -iname file_name.foo', :light_blue)}


    #{colorize('Connect to WiFi via command line without GUI installed:', :red)}

    - First, set the wireless link up. To get the name (and status) of the link:

    $ #{colorize('  ip link', :light_blue)}

    - The name will be something like: `wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP>`.
    If it doesn't say "UP" between the brackets, set it up:

    $ #{colorize('  ip link set wlan0 up', :light_blue)}

    - Then we need to start the `iwd` service (this needs to be installed beforehand
    via pacman; the Arch ISO will always have the tools available to connect to wifi
    if pacman is needed on the nnew install)

    $ #{colorize('  systemctl start iwd.service', :light_blue)}

    - Now we use the `iwctl` to get an interactive prompt. Then we scan for networks, and
    attach to the one we want:

    [iwd]# #{colorize('  station wlan0 scan', :light_blue)}
    [iwd]# #{colorize('  station wlan0 connect Mertaugh-5GHz', :light_blue)}
    [iwd]# #{colorize('  quit', :light_blue)}

    - Lastly, we start the the DHCP client:

    $ #{colorize('  systemctl start dhcpcd@wlan0.service', :light_blue)}

    - We should now be connected to the network:

    $ #{colorize('  ping www.archlinux.org', :light_blue)}

    HEREDOC
    end
  end

  ProgrammingTasks.print
