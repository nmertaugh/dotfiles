# Variables
# SOURCE="${BASH_SOURCE[0]}"
# while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
#   DIR="$(cd -P "$(dirname "$SOURCE")" && pwd)"
#   SOURCE="$(readlink "$SOURCE")"
#   [[ $SOURCE != /* ]] &&z SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
# done
# DIR="$(cd -P "$(dirname "$SOURCE")" && pwd)"

# Load Dependencies
if [ -f "${HOME}/.bash_aliases" ]; then
  source "${HOME}/.bash_aliases"
fi

if [ -f "${HOME}/.bash_functions" ]; then
  source "${HOME}/.bash_functions"
fi

# For Buoy work
export JAVA_HOME='/usr/lib/jvm/default'
