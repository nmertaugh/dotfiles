#!/bin/bash

# Main Method Definition
establish_symlinks() {
  local -n files=$1
  local target_path=$2

  printf "\nLinking ${target_path}:\n\n"

  for ((i=0; i<${#files[*]}; i++));
  do
    local target=${target_path}/${files[i]}
    local dotfile_path=/home/nmertaugh/Development/personal/dotfiles

    printf "  ${files[i]}\n"

    if [ -f ${target} ];
    then
      rm ${target}
    fi

    ln -s ${dotfile_path}/${dotfiles[i]} ${target}
    chmod 644 ${target}
  done
}

# Home Directory
target_path=/home/nmertaugh
dotfiles=(
  .bash_aliases
  .bash_functions
  .bash_profile
  .bashrc
  .dir_colors
  .gemrc
  .gitconfig
  .gitignore
  .rspec
  .curlrc
)
establish_symlinks dotfiles ${target_path}

# ~/.config Directory
printf "\nCopying to ~/.config:\n"
printf "  yt-dlp/config\n"
ln -s /home/nmertaugh/Development/personal/dotfiles/.config/yt-dlp/config /home/nmertaugh/.config/yt-dlp/config

# /etc Directory
printf "\nCopying to /etc:\n"
printf "  pacman.conf\n"
sudo rm /etc/pacman.conf
sudo ln -s /home/nmertaugh/Development/personal/dotfiles/pacman.conf /etc/pacman.conf

# VS Code Config
ln -s /home/nmertaugh/Development/personal/dotfiles/vscode/keybindings.json /home/nmertaugh/.config/Code\ -\ OSS/User/keybindings.json
ln -s /home/nmertaugh/Development/personal/dotfiles/vscode/settings.json /home/nmertaugh/.config/Code\ -\ OSS/User/settings.json
ln -s /home/nmertaugh/Development/personal/dotfiles/vscode/snippets/ruby.json /home/nmertaugh/.config/Code\ -\ OSS/User/snippets/ruby.json

# Boot Directory (no symlinks permitted in /boot)
printf "\nCopying to /boot:\n"
printf "  README.txt\n"
sudo cp /home/nmertaugh/Development/personal/dotfiles/.boot/README.txt /boot/README.txt

# Done
printf "\nDone"
